package com.penngo.common;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import com.jfinal.core.JFinal;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.penngo.util.Tool;

public class Main {
	public static void initDb() {
		PropKit.use("config.txt");
		System.out.println(PropKit.get("jdbcUrl"));

		try {
			Class.forName("org.sqlite.JDBC");
			Connection conn =
					DriverManager.getConnection(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password").trim());

		    Statement stat = conn.createStatement();
		    //stat.executeUpdate("drop table if exists people;");
//		    stat.execute("");//.executeUpdate("create table people (name, occupation);");
			ResultSet rs = stat.executeQuery(" SELECT `name` FROM `sqlite_master` WHERE `type`='table'; ");
			Map<String, Boolean> existTable = new HashMap<>();
			while (rs.next()) {
				String name = rs.getString("name");
				existTable.put(name, true);
			}
			if(existTable.size() < 2){
				String[] sqls = Tool.getSql();
				//System.out.println("configPlugin=========" + sql);
				//Connection connection = dataSource.getConnection();

				for(String sql:sqls) {
					System.out.println("configPlugin=========" + sql);
					int b = conn.createStatement().executeUpdate(sql);
					//Boolean b = connection.prepareStatement(sql).execute();
					//Boolean b = stat.execute(sql);
					System.out.println("configPlugin=========" + b);
				}
			}

			conn.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		//System.out.println("args=====" + args.length);
		initDb();
		int port = 8000; // 默认端口
		if(args.length == 1){
			port = Integer.valueOf(args[0]);
		}
		String root = new File("").getAbsolutePath();
		System.out.println("root=" + root);
		PathKit pathKit = new PathKit();
		File file = new File("webapp");
		if(file.exists() == true) {
			pathKit.setWebRootPath(root + "/webapp");
//			JFinal.start("src/main/webapp", port, "/", 5); //eclipse
//			JFinal.start("webapp", port, "/");  //idea
			JFinal.start("webapp", port, "/",5);
		}
		else {
			pathKit.setWebRootPath(root + "/src/main/webapp");
//			JFinal.start("src/main/webapp", port, "/", 5); //eclipse
			//JFinal.start("src/main/webapp", port, "/");  //idea
			JFinal.start("src/main/webapp", port, "/",5);
		}


	}
}
